# Medium-editor for d8
Get a medium editor clone inside Drupal using https://github.com/daviferreira/medium-editor

## Library
Library is included.

## Install
Should be straight forward. Two settings is included in the install that could be used.

## @todo
There are some basics things to make this more useful, like adding image handling.


